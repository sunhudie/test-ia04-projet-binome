package main

import (
	"fmt"
	ras "gitlab.utc.fr/zhenyang/ia04/td5/vote/restserveragent"
)

func main() {
	server := ras.NewRestServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
